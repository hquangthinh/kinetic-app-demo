import { Component, Query, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IEpViewModel,
  EpErpRestService,
  EpToastType,
  EpToastService,
  EpRestErrorType,
  EpErpOdataService,
  EpApplicationService,
  EpDataView,
  EpDataViewModelService,
  EpButtonSize,
  IEpErpErrorResponse,
  IEpToastOptions,
  EpFeatureDetectionService,
  EpMobileMapService,
  EpMobileGeolocationService,
  EpLogService,
  IEpConfigGeolocation,
  EpTransitionType,
  IEpGeofenceOptions,
  EpEventService
} from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';
import { AppLocalizationStrings } from '../app.localization.strings';
import { AttachmentComponent } from '@progress/kendo-angular-conversational-ui/dist/es2015/chat/attachment.component';

@Component({
  selector: 'app-service-job-detail',
  templateUrl: './service-job-detail.component.html',
  styleUrls: ['./service-job-detail.component.scss']
})
export class ServiceJobDetailComponent implements OnInit {
  EpButtonSize = EpButtonSize;
  viewModel: IEpViewModel = {
    contextBar: {
      title: 'ServiceJobDetail view.'
    }
  };
  trx: EpDataViewModelService = EpDataViewModelService.current;
  dataView: EpDataView = new EpDataView();
  epApplicationService = EpApplicationService.current;
  jobNum: string;
  jobModel: any = {
    StartDate: new Date(),
    DueDate: new Date(),
    // tslint:disable-next-line: no-construct
    Signature: new String()
  };
  jobOperationModel: any = {
    StartDate: new Date(),
    DueDate: new Date()
  };

  signatureName: string;
  isSignatureCreated: boolean;
  imageModel: any = {
    Company: 'EPIC06',
    ImageID: '',
    ImageFileName: '',
    ImageFileThumbnail: '',
    ImageCategoryID: '',
    ImageSubCategoryID: '',
    CreatedBy: '',
    CreatedOn: new Date(),
    ModifiedBy: '',
    ModifiedOn: new Date(),
    FileSize: 0,
    Width: '0',
    Height: '0',
    FileType: '',
    GlobalImage: true,
    SysRevID: '0',
    SysRowID: '00000000-0000-0000-0000-000000000000',
    Reference1: '',
    Reference2: '',
    Reference3: '',
    Reference4: '',
    Reference5: '',
    ThumbnailSysRowID: '00000000-0000-0000-0000-000000000000',
    ImageSysRowID: '00000000-0000-0000-0000-000000000000',
    ImageCategoryDesc: '',
    ImageContent: '',
    ImageSubCategoryDesc: '',
    ThumbnailContent: '',
    BitFlag: 0,
    RowMod: ''
  };
  errorMsg: string;
  isMobile = false;
  map: any;
  currentLocation: any;
  watch: any;
  options: IEpGeofenceOptions;
  config: IEpConfigGeolocation = {
    desiredAccuracy: 0,
    stationaryRadius: 20,
    distanceFilter: 10,
    debug: true
  };

  signOptions = {
    canvasHeight: 200,
    canvasWidth: 800,
    backgroundColor: 'lightgray',
    penColor: 'gray',
    showSavePngButton: false,
    showSaveJpgButton: false,
    showClearButton: true,
    showAcceptButton: true,
    value: ''
  };

  constructor(
    private epFeatureDetectionService: EpFeatureDetectionService,
    private epMobileMapService: EpMobileMapService,
    private epMobileGeolocationService: EpMobileGeolocationService,
    private epLogService: EpLogService,
    private appCommonService: AppCommonService,
    private epRestService: EpErpRestService,
    private epToast: EpToastService,
    private route: ActivatedRoute,
    private router: Router,
    private epFeature: EpFeatureDetectionService,
    private epEvent: EpEventService,
    elementRef: ElementRef
  ) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = 'Service Job: ' + this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.jobNum = this.route.snapshot.paramMap.get('id');
    this.signatureName = this.jobNum + '_signature';
    this.getServiceJobDetail(this.jobNum);
    this.getJobOpers(this.jobNum);
    this.loadMap();
    this.getSignature(this.signatureName);
  }

  getSignature(imageId) {
    const query = new EpErpOdataService();
    const params: any = {
      ImageId: imageId
    };
    query.setTop(1);
    this.epApplicationService.restService.get('Erp.BO.ImageSvc', 'Images', { showProgress: false, query: query }).subscribe(resultData => {
      if (resultData.length > 0) {
        this.isSignatureCreated = true;
        this.imageModel = resultData[0];
        this.signOptions.value = 'data:image/png;base64,' + this.imageModel.ImageContent;
      } else {
        this.isSignatureCreated = false;
      }
    });
  }

  updateSignature() {
    this.imageModel.ImageContent = this.signOptions.value.replace('data:image/png;base64,', '');
    if (this.isSignatureCreated) {
      this.epRestService
        .patch('Erp.BO.ImageSvc', 'Images', this.imageModel)
        .toPromise()
        .then(
          data => {
            console.log('save done ', data);
          },
          error => {
            this.errorMsg = error.message;
            this.showMessage(EpToastType.Error, error.message);
          }
        );
    } else {
      this.imageModel.ImageId = this.signatureName;
      this.imageModel.ImageFileName = this.signatureName + '.png';

      this.epRestService
        .post('Erp.BO.ImageSvc', 'Images', this.imageModel)
        .toPromise()
        .then(
          data => {
            console.log('save signature done ');
          },
          error => {
            this.errorMsg = error.message;
            this.showMessage(EpToastType.Error, error.message);
          }
        );
    }
  }

  getServiceJobDetail(jobNum) {
    const query = new EpErpOdataService();
    const params: any = {
      JobNum: jobNum
    };
    query.setFilter(params);
    this.epApplicationService.restService
      .get('Erp.BO.JobEntrySvc', 'JobEntries', { showProgress: true, query: query })
      .subscribe(resultData => {
        console.log('JobEntries ', resultData);
        this.jobModel = resultData[0];
        console.log('JobModel: ', this.jobModel);
        if (this.jobModel.StartDate) {
          this.jobModel.StartDate = new Date(this.jobModel.StartDate);
        }
        if (this.jobModel.DueDate) {
          this.jobModel.DueDate = new Date(this.jobModel.DueDate);
        }
        this.dataView = EpDataViewModelService.current.add('jobModel', this.jobModel);
        this.trx.addView(this.dataView);
      });
  }

  getJobOpers(jobNum) {
    const query = new EpErpOdataService();
    query.setTop(50);
    const params: any = {
      JobNum: jobNum,
      JobComplete: false
    };
    query.setFilter(params);
    this.epApplicationService.restService
      .get('Erp.BO.JobEntrySvc', 'JobOpers', { showProgress: true, query: query })
      .subscribe(resultData => {
        console.log('JobOpers ', resultData);
        this.jobOperationModel = resultData;
      });
  }

  saveJobDetail() {
    console.log('saving job detail... ', this.jobModel);
    console.log(this.currentLocation);
    if (this.currentLocation) {
      this.jobModel.UserChar1 = `${this.currentLocation.lat},${this.currentLocation.lng}`;
    }
    return this.epRestService
      .patch('Erp.BO.JobEntrySvc', 'JobEntries', this.jobModel, {
        pathParams: ['EPIC06', this.jobModel.JobNum]
      })
      .subscribe(_ => {
        this.showMessage(EpToastType.Success, `Job Updated Successfully`);
      });
  }

  saveJobOperations() {
    console.log('saving... ', this.jobOperationModel);

    this.updateSignature();

    if (Array.isArray(this.jobOperationModel)) {
      const result: Promise<any>[] = [];
      for (let i = 0; i < this.jobOperationModel.length; i++) {
        const res = this.saveJobOperation(this.jobOperationModel[i]).toPromise();
        result.push(res);
      }
      Promise.all(result).then(
        data => {
          console.log('save done ');
          this.showMessage(EpToastType.Success, `Job Operations Updated Successfully`);
        },
        error => {
          this.errorMsg = error.message;
          this.showMessage(EpToastType.Error, error.message);
        }
      );
    }
  }

  saveJobOperation(jobOperationModel) {
    return this.epRestService.patch('Erp.BO.JobEntrySvc', 'JobOpers', jobOperationModel, {
      pathParams: ['EPIC06', this.jobNum, jobOperationModel.AssemblySeq, jobOperationModel.OprSeq]
    });
  }

  showMessage(toastType: EpToastType, message: string) {
    this.epToast.show({
      toastConfig: {
        newestOnTop: true,
        progressBar: true,
        progressAnimation: 'decreasing',
        timeOut: 3000,
        extendedTimeOut: 3000,
        positionClass: 'toast-top-right',
        closeButton: true,
        enableHtml: true,
        tapToDismiss: true,
        preventDuplicates: false,
        resetTimeoutOnDuplicate: false,
        disableTimeOut: false,
        maxOpened: 0,
        autoDismiss: false,
        easeTime: 30,
        toastClass: 'toast'
      },
      toastType: toastType,
      title: toastType === EpToastType.Error ? 'Error' : 'Success',
      message: message
    });
  }

  loadMap() {
    this.isMobile = this.epFeatureDetectionService.features.isCordova;
    if (this.isMobile) {
      const div = document.getElementById('map_canvas');
      const options = {
        controls: {
          compass: true,
          myLocationButton: true,
          myLocation: true
        }
      };
      this.epMobileMapService.getMap(div, options, this.onMapSuccess.bind(this), this.onMapError.bind(this));
    }
  }

  startTracking() {
    this.epMobileGeolocationService.startTracking(this.config, this.onSuccess.bind(this), this.onFail.bind(this));
  }

  onSuccess() {
    this.epLogService.info('Tracking success');
    const options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    this.watch = navigator.geolocation.watchPosition(this.onWatchSuccess.bind(this), this.onWatchFail.bind(this), options);
  }

  onWatchSuccess(position: any) {
    this.epLogService.info(position);

    // tslint:disable-next-line:max-line-length
    const iconBase64Img =
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAE0lEQVR42mNkWPn/JwMWwDiQEgA16Q/NNvoJdAAAAABJRU5ErkJggg==';

    const markerOptions = {
      position: {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      },
      icon: iconBase64Img
    };

    this.epMobileMapService.addMarker(this.map, markerOptions);
  }

  onWatchFail(error: any) {
    this.epLogService.info('Watch failed' + error);
  }

  onFail(err: any) {
    this.epLogService.info('Tracking failed' + err);
  }

  stopTracking() {
    this.epMobileGeolocationService.stopTracking(this.watch);
  }

  private onMapSuccess(mapObj: any) {
    this.map = mapObj;
    mapObj.getMyLocation().then((location: any) => {
      // Move the map camera to the location without animation
      mapObj.moveCamera({
        target: location.latLng,
        zoom: 16,
        tilt: 30,
        bearing: 45
      });
      this.currentLocation = location.latLng;
    });
  }

  private onMapError(error: any) {
    this.epLogService.error('Failed because: ' + error);
  }
}
