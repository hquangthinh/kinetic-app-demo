import { Component, OnInit, ViewChild } from '@angular/core';
import {
  IEpViewModel,
  EpFeatureDetectionService,
  EpMobileMapService,
  EpMobileGeolocationService,
  EpLogService,
  IEpConfigGeolocation,
  EpTransitionType,
  EpButtonSize,
  IEpGeofenceOptions
} from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';

declare let plugin: any;
declare let map: any;

@Component({
  selector: 'app-geolocation-map-page',
  templateUrl: './geolocation-map-page.component.html',
  styleUrls: ['./geolocation-map-page.component.scss']
})
export class GeolocationMapPageComponent implements OnInit {
  viewModel: IEpViewModel = {
    contextBar: {
      title: 'GeolocationMapPage view.'
    }
  };
  isMobile = false;
  map: any;
  watch: any;
  options: IEpGeofenceOptions;

  EpButtonSize = EpButtonSize;

  config: IEpConfigGeolocation = {
    desiredAccuracy: 0,
    stationaryRadius: 20,
    distanceFilter: 10,
    debug: true
  };

  constructor(
    private epFeatureDetectionService: EpFeatureDetectionService,
    private epMobileMapService: EpMobileMapService,
    private epMobileGeolocationService: EpMobileGeolocationService,
    private epLogService: EpLogService,
    private appCommonService: AppCommonService
  ) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = 'EpGeolocation with Google map';
  }

  ngOnInit() {
    this.isMobile = this.epFeatureDetectionService.features.isCordova;
    if (this.isMobile) {
      const div = document.getElementById('map_canvas');
      const options = {
        controls: {
          compass: true,
          myLocationButton: true,
          myLocation: true
        }
      };
      this.epMobileMapService.getMap(div, options, this.onMapSuccess.bind(this), this.onMapError.bind(this));
    }
  }

  private onMapSuccess(mapObj: any) {
    this.map = mapObj;
    mapObj.getMyLocation().then((location: any) => {
      // Move the map camera to the location without animation
      mapObj.moveCamera({
        target: location.latLng,
        zoom: 16,
        tilt: 30,
        bearing: 45
      });
    });
  }

  private onMapError(error: any) {
    this.epLogService.error('Failed because: ' + error);
  }

  startTracking() {
    this.epMobileGeolocationService.startTracking(this.config, this.onSuccess.bind(this), this.onFail.bind(this));
  }

  onSuccess() {
    this.epLogService.info('Tracking success');
    const options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    this.watch = navigator.geolocation.watchPosition(this.onWatchSuccess.bind(this), this.onWatchFail.bind(this), options);
  }

  onWatchSuccess(position: any) {
    this.epLogService.info(position);

    // tslint:disable-next-line:max-line-length
    // const iconBase64Img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAiklEQVR42mNgQIAoIF4NxGegdCCSHAMzEC81M4v6n56++n9V1RkwbWgY+B8oPhOmKM3WNu3/zJn/MbCFRSxIYSxI0YHi4gNYFRUW7gUp2gtS9LC9/SFWRc3Nt0GKbhNtUizIbmyKjIxCQIpCYI6fD/JdVtZGsO8yMtbBfDeNAQ2AwmkjNJzWIYcTAMk+i9OhipcQAAAAAElFTkSuQmCC';
    // tslint:disable-next-line:max-line-length
    const iconBase64Img =
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAE0lEQVR42mNkWPn/JwMWwDiQEgA16Q/NNvoJdAAAAABJRU5ErkJggg==';

    const markerOptions = {
      position: {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      },
      icon: iconBase64Img
    };

    this.epMobileMapService.addMarker(this.map, markerOptions);
  }

  onWatchFail(error: any) {
    this.epLogService.info('Watch failed' + error);
  }

  onFail(err: any) {
    this.epLogService.info('Tracking failed' + err);
  }

  stopTracking() {
    this.epMobileGeolocationService.stopTracking(this.watch);
  }

  addGeofence() {
    this.map.getMyLocation().then((location: any) => {
      this.epLogService.info(location);
      this.options = {
        id: '1',
        latitude: location.latLng.lat,
        longitude: location.latLng.lng,
        radius: 300,
        transitionType: EpTransitionType.EXIT,
        notification: {
          id: 1,
          title: 'Bye Bye',
          text: 'You just left the premises',
          openAppOnClick: true
        }
      };

      this.epMobileGeolocationService.addOrUpdateGeofence(this.map, this.options);
    });
  }

  removeGeofence() {
    this.epMobileGeolocationService.removeAll();
  }
}
