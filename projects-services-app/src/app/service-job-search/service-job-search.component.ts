import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEpViewModel, EpErpOdataService, EpApplicationService, IEpSearchBox, EpSearchBoxComponent } from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';
import { AppLocalizationStrings } from '../app.localization.strings';

@Component({
  selector: 'app-service-job-search',
  templateUrl: './service-job-search.component.html',
  styleUrls: ['./service-job-search.component.scss']
})
export class ServiceJobSearchComponent implements OnInit {
  viewModel: IEpViewModel;
  appStrings = AppLocalizationStrings;
  epApplicationService = EpApplicationService.current;
  listData: Object[];
  searchString: string;

  searchModel: IEpSearchBox = {
    debounceTime: 200,
    placeholder: 'Search..',
    onChange: str => {
      this.searchString = str;
      this.getListData(this.searchString);
    }
  };

  constructor(private appCommonService: AppCommonService, private router: Router) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = this.appCommonService.appStrings.serviceJobs;
  }

  ngOnInit() {
    this.getListData(this.searchString);
  }

  getListData(searchKeyword) {
    const query = new EpErpOdataService();
    query.setTop(50);
    if (searchKeyword && searchKeyword !== '') {
      query.setFilter({
        JobNum: searchKeyword,
        JobClosed: false
      });
    }

    this.epApplicationService.restService.get('Erp.BO.JobEntrySvc', 'JobEntries', { query: query }).subscribe(resultData => {
      this.listData = resultData;
      // console.log(resultData);
    });
  }

  viewItemDetail(item) {
    console.log(item);
    this.router.navigate(['service-job-detail/' + item.JobNum]).then(e => {});
  }

  editItemDetail(item) {}

  deleteItem(item) {}
}
