export enum AppLocalizationStrings {
  $id = 'app.localization',
  about = 'About',
  aboutDesc = 'About the application',
  home = 'Home',
  homeDesc = 'The application home page',
  serviceJobs = 'Service Jobs',
  serviceJobsDesc = 'Service Jobs page'
}
