import { Component, OnInit } from '@angular/core';
import { IEpViewModel } from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  viewModel: IEpViewModel;

  constructor(private appCommonService: AppCommonService) {
    this.viewModel = this.appCommonService.getViewModel();
  }

  ngOnInit() {}
}
