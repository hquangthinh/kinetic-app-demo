import { Component, OnInit } from '@angular/core';
import { IEpViewModel } from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';

@Component({
  selector: 'app-cordova-geolocation-map-page',
  templateUrl: './cordova-geolocation-map-page.component.html',
  styleUrls: ['./cordova-geolocation-map-page.component.scss']
})
export class CordovaGeolocationMapPageComponent implements OnInit {
  viewModel: IEpViewModel = {
    contextBar: {
      title: 'CordovaGeolocationMapPage view.'
    }
  };
  message = '';

  constructor(private appCommonService: AppCommonService) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = 'Cordova Geo Location';
  }

  ngOnInit() {
    navigator.geolocation.getCurrentPosition(this.onSuccess.bind(this), this.onError.bind(this));
  }

  // onSuccess Callback
  // This method accepts a Position object, which contains the
  // current GPS coordinates
  //
  onSuccess(position) {
    this.message =
      'Latitude: ' +
      position.coords.latitude +
      '\n' +
      'Longitude: ' +
      position.coords.longitude +
      '\n' +
      'Altitude: ' +
      position.coords.altitude +
      '\n' +
      'Accuracy: ' +
      position.coords.accuracy +
      '\n' +
      'Altitude Accuracy: ' +
      position.coords.altitudeAccuracy +
      '\n' +
      'Heading: ' +
      position.coords.heading +
      '\n' +
      'Speed: ' +
      position.coords.speed +
      '\n' +
      'Timestamp: ' +
      position.timestamp +
      '\n';
    console.log(this.message);
  }

  // onError Callback receives a PositionError object
  //
  onError(error) {
    this.message = 'code: ' + error.code + '\n' + 'message: ' + error.message + '\n';
    console.log(this.message);
  }
}
