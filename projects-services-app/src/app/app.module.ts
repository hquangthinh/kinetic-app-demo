import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import {
  EpCore2Module,
  EpLoginComponent,
  EpLocalizationService,
  EpApplicationModule,
  EpShellModule,
  EpLoginModule,
  EpEulaModule,
  EpGridModule,
  EpButtonsModule,
  EpErpLoginModule,
  EpInputsModule,
  EpBreadcrumbModule,
  EpFloatingActionButtonModule,
  EpCheckBoxModule,
  EpDateInputsModule,
  EpSearchBoxModule,
  EpSignatureModule
} from '@epicor/kinetic';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { AppLocalizationStrings } from './app.localization.strings';
import { AboutComponent } from './about/about.component';
import { AppCommonService } from './app.common.service';
import { ServiceJobsComponent } from './service-jobs/service-jobs.component';
import { ServiceJobSearchComponent } from './service-job-search/service-job-search.component';
import { ServiceJobDetailComponent } from './service-job-detail/service-job-detail.component';
import { ProjectSearchComponent } from './project-search/project-search.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { GeolocationMapPageComponent } from './geolocation-map-page/geolocation-map-page.component';
import { FeatureDetectionPageComponent } from './feature-detection-page/feature-detection-page.component';
import { CordovaGeolocationMapPageComponent } from './cordova-geolocation-map-page/cordova-geolocation-map-page.component';

/**
 * Register the application localization strings to be translated.
 */
EpLocalizationService.registerStrings(AppLocalizationStrings);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LeftSidebarComponent,
    AboutComponent,
    ServiceJobsComponent,
    ServiceJobSearchComponent,
    ServiceJobDetailComponent,
    ProjectSearchComponent,
    ProjectDetailComponent,
    GeolocationMapPageComponent,
    FeatureDetectionPageComponent,
    CordovaGeolocationMapPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    EpLoginModule,
    EpShellModule,
    EpApplicationModule,
    EpEulaModule,
    EpCore2Module.forRoot(),
    EpErpLoginModule,
    EpGridModule,
    EpButtonsModule,
    EpInputsModule,
    EpBreadcrumbModule,
    EpFloatingActionButtonModule,
    EpCheckBoxModule,
    EpDateInputsModule,
    EpSearchBoxModule,
    EpSignatureModule
  ],
  providers: [AppCommonService],
  bootstrap: [AppComponent],
  entryComponents: [EpLoginComponent, LeftSidebarComponent]
})
export class AppModule {}
