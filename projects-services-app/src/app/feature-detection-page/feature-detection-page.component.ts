import { Component, OnInit, ViewChild } from '@angular/core';
import { EpFeatureDetectionService, IEpViewModel } from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';

@Component({
  selector: 'app-feature-detection-page',
  templateUrl: './feature-detection-page.component.html',
  styleUrls: ['./feature-detection-page.component.scss']
})
export class FeatureDetectionPageComponent implements OnInit {
  viewModel: IEpViewModel = {
    contextBar: {
      title: 'FeatureDetectionPage view.'
    }
  };
  deviceData;

  constructor(epFeatureDetection: EpFeatureDetectionService, private appCommonService: AppCommonService) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = 'Features Detection';
    this.deviceData = epFeatureDetection.getFeatures();
  }

  ngOnInit(): void {}
}
