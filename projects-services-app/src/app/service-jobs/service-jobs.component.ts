import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {
  IEpViewModel,
  EpErpOdataService,
  EpApplicationService,
  IEpGridModel,
  EpDataView,
  EpDataViewModelService,
  EpTextBoxComponent,
  IEpBreadcrumb,
  EpGridComponent
} from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';
import { AppLocalizationStrings } from '../app.localization.strings';
import { Router } from '@angular/router';

@Component({
  selector: 'app-service-jobs',
  templateUrl: './service-jobs.component.html',
  styleUrls: ['./service-jobs.component.scss']
})
export class ServiceJobsComponent implements OnInit {
  viewModel: IEpViewModel;
  appStrings = AppLocalizationStrings;
  epApplicationService = EpApplicationService.current;
  gridModel: IEpGridModel;
  trx: EpDataViewModelService = EpDataViewModelService.current;
  dataView: EpDataView = new EpDataView();
  listData: Object[];
  @ViewChild('customCellTemplate') customCellTemplate: TemplateRef<any>;

  constructor(private appCommonService: AppCommonService, private router: Router) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = this.appCommonService.appStrings.serviceJobs;
  }

  ngOnInit() {
    this.initGridModel();
    this.getListData();
  }

  initGridModel() {
    const model: IEpGridModel = {
      resizable: true,
      reorderable: true,
      groupable: false,
      filterable: true,
      sortable: true,
      pageable: false,
      selectable: true,
      editable: false,
      epBinding: 'list',
      height: 550,
      columns: [
        { field: 'JobNum', title: 'ID', width: 50, hidden: false, cellTemplate: this.customCellTemplate },
        { field: 'PartDescription', title: 'Descrition', width: 250, autoSize: false },
        { field: 'ReqDueDate', title: 'Due', width: 150, editable: true, editor: 'date' }
      ]
    };
    this.gridModel = model;
  }

  getListData() {
    const query = new EpErpOdataService();
    query.setTop(50);

    this.epApplicationService.restService.get('Erp.BO.JobEntrySvc', 'JobEntries', { query: query }).subscribe(resultData => {
      this.listData = resultData;
      this.dataView = EpDataViewModelService.current.add('list', this.listData);
      this.trx.addView(this.dataView);
    });
  }

  onEdit() {
    console.log('onEdit ', arguments);
  }

  cellClick() {
    console.log('onCellClick 1');
    // this.router.navigate(['service-job-detail/' + item.JobNum]).then(e => {});
  }

  onDetailExpand(item) {}
}
