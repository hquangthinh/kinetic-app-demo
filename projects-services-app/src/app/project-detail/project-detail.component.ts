import { Component, Query, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IEpViewModel,
  EpErpRestService,
  EpToastType,
  EpToastService,
  EpRestErrorType,
  EpErpOdataService,
  EpApplicationService,
  EpDataView,
  EpDataViewModelService,
  EpButtonSize,
  IEpErpErrorResponse,
  IEpToastOptions,
  IEpConfigToast
} from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  EpButtonSize = EpButtonSize;
  viewModel: IEpViewModel = {
    contextBar: {
      title: 'Project view.'
    }
  };
  trx: EpDataViewModelService = EpDataViewModelService.current;
  dataView: EpDataView = new EpDataView();
  epApplicationService = EpApplicationService.current;
  projectId: string;
  projectModel: any = {
    StartDate: new Date()
  };
  errorMsg: string;

  tstConfig: IEpConfigToast = {
    newestOnTop: true,
    progressBar: true,
    progressAnimation: 'decreasing',
    timeOut: 3000,
    extendedTimeOut: 3000,
    positionClass: 'toast-top-right',
    closeButton: true,
    enableHtml: true,
    tapToDismiss: true,
    preventDuplicates: false,
    resetTimeoutOnDuplicate: false,
    disableTimeOut: false,
    maxOpened: 0,
    autoDismiss: false,
    easeTime: 30,
    toastClass: 'toast'
  };

  constructor(
    private appCommonService: AppCommonService,
    private epRestService: EpErpRestService,
    private epToast: EpToastService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = 'Project: ' + this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get('id');
    this.getProjectDetail(this.projectId);
  }

  getProjectDetail(projectId) {
    this.epRestService
      .get('Erp.BO.ProjectSvc', 'Projects', {
        showProgress: true,
        pathParams: ['EPIC06', projectId]
      })
      .subscribe(resultData => {
        console.log('Projects detail ', resultData);
        this.projectModel = resultData[0];
        this.projectModel.StartDate = new Date(this.projectModel.StartDate);
      });
  }

  save() {
    console.log('saving... ', this.projectModel);
    this.epRestService.patch('Erp.BO.ProjectSvc', 'Projects', this.projectModel, { pathParams: ['EPIC06', this.projectId] }).subscribe(
      data => {
        console.log('save done ', data);
        this.showMessage(EpToastType.Success, 'Project Updated Successfully');
      },
      (error: IEpErpErrorResponse) => {
        this.errorMsg = error.message;
        this.showMessage(EpToastType.Error, error.message);
      }
    );
  }

  delete() {}

  cancel() {
    this.router.navigate(['project-search']);
  }

  showMessage(toastType: EpToastType, message: string) {
    this.epToast.show({
      toastConfig: this.tstConfig,
      toastType: toastType,
      title: toastType === EpToastType.Error ? 'Error' : 'Success',
      message: message
    });
  }
}
