import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ServiceJobsComponent } from './service-jobs/service-jobs.component';
import { ServiceJobSearchComponent } from './service-job-search/service-job-search.component';
import { ServiceJobDetailComponent } from './service-job-detail/service-job-detail.component';
import { ProjectSearchComponent } from './project-search/project-search.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { GeolocationMapPageComponent } from './geolocation-map-page/geolocation-map-page.component';
import { FeatureDetectionPageComponent } from './feature-detection-page/feature-detection-page.component';
import { CordovaGeolocationMapPageComponent } from './cordova-geolocation-map-page/cordova-geolocation-map-page.component';

/*
 * Since we're using integrated login flow via the sysconfig.json "enableLoginFlow" setting,
 * we don't need to add auth guard and login path data.
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'service-jobs',
    component: ServiceJobsComponent
  },
  { path: 'service-job-search', component: ServiceJobSearchComponent },
  { path: 'service-job-detail/:id', component: ServiceJobDetailComponent },
  { path: 'project-search', component: ProjectSearchComponent },
  { path: 'project-detail/:id', component: ProjectDetailComponent },
  { path: 'geolocation-map', component: GeolocationMapPageComponent },
  { path: 'feature-detection', component: FeatureDetectionPageComponent },
  { path: 'cordova-geolocation-map', component: CordovaGeolocationMapPageComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
