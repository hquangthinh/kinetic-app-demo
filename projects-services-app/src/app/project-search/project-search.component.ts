import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IEpViewModel, EpErpOdataService, EpApplicationService, IEpSearchBox, EpSearchBoxComponent } from '@epicor/kinetic';
import { AppCommonService } from '../app.common.service';
import { AppLocalizationStrings } from '../app.localization.strings';

@Component({
  selector: 'app-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.scss']
})
export class ProjectSearchComponent implements OnInit {
  viewModel: IEpViewModel;
  appStrings = AppLocalizationStrings;
  epApplicationService = EpApplicationService.current;
  listData: Object[];
  searchString: string;

  searchModel: IEpSearchBox = {
    debounceTime: 200,
    placeholder: 'Search..',
    onChange: str => {
      this.searchString = str;
      this.getListData(this.searchString);
    }
  };

  constructor(private appCommonService: AppCommonService, private router: Router) {
    this.viewModel = this.appCommonService.getViewModel();
    this.viewModel.contextBar.title = this.appCommonService.appStrings.serviceJobs;
  }

  ngOnInit() {
    this.getListData(this.searchString);
  }

  getListData(searchKeyword) {
    const query = new EpErpOdataService();
    query.setTop(20);
    if (searchKeyword && searchKeyword !== '') {
      query.setFilter({
        CompanyID: 'EPIC06',
        ProjectID: searchKeyword
      });
    }

    this.epApplicationService.restService.get('Erp.BO.ProjectSvc', 'Projects', { query: query }).subscribe(resultData => {
      this.listData = resultData;
      // console.log(resultData);
    });
  }

  viewItemDetail(item) {
    this.router.navigate(['project-detail/' + item.ProjectID]).then(e => {});
  }

  editItemDetail(item) {}

  deleteItem(item) {}
}
