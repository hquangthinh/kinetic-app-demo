import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EpCore2Module, EpShellModule } from '@epicor/kinetic';
import { RouterTestingModule } from '@angular/router/testing';
import { ProjectSearchComponent } from './project-search.component';

describe('ProjectSearchComponent', () => {
  let component: ProjectSearchComponent;
  let fixture: ComponentFixture<ProjectSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [EpCore2Module.forRoot(), EpShellModule, RouterTestingModule],
      declarations: [ProjectSearchComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
